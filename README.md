# eQTG-Finder

This is a repository for eQTG-Finder, an eQTL gene prioritization tool based on  [QTG-Finder](https://github.com/carnegie/QTG_Finder).

## Usage
eQTG-Finder prediction is integrated into [AraQTL](https://www.bioinformatics.nl/AraQTL/). Please follow the website manual and example to use eQTG-Finder to find regulators for gene of interest. 

## Dependency
Packages used for eQTG-Finder analyses are listed in requirements.txt. You can create a conda evironment containing the required packages using:
```sh
conda create --name <env> --file requirements.txt
```
