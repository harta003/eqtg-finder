# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 15:13:36 2022

@author: harta005
"""

# Load libraries
import os
os.chdir('C://Users/harta005/Projects/eQTG_Finder/analysis')
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

# Main code
df = pd.read_csv('../data/eQTG_features.csv')
df = df.drop(['class'], axis=1)
qtg = pd.read_csv('../data/gene_ranks.csv')
qtg = qtg[(qtg.model == 'eQTG_Finder') &
          (qtg.type == 'eQTG')]
qtg['evidence'] = qtg['evidence'].fillna('QTG')
df #= df[df.ID.isin(qtg.id)]

# Tidy up the data
x = df.drop(['ID'], axis=1)
y = df['ID'].values
x = StandardScaler().fit_transform(x)

# Create PCA
pca = PCA(n_components=2)
pc = pca.fit_transform(x)
pc_df = pd.DataFrame(data = pc,
                     columns = ['PC1', 'PC2'])
pc_df = pd.concat([pc_df, df.reset_index()['ID']], axis=1, ignore_index=True)
pc_df.columns = ['PC1', 'PC2', 'id']
pc_df = pd.merge(pc_df, qtg, on='id', how='outer')
pc_df = pc_df.fillna('else')

# Plot
fig = plt.figure(figsize = (8, 8))
ax = fig.add_subplot(1, 1, 1)
ax.set_xlabel('Principal Component 1', fontsize = 10)
ax.set_ylabel('Principal Component 2', fontsize = 10)

ax.grid()
targets = pc_df.evidence.unique()
colors = ['y', 'g', 'r', 'b']
for target, color in zip(targets,colors):
    indicesToKeep = pc_df['evidence'] == target
    plt.scatter(pc_df.loc[indicesToKeep, 'PC1'],
                pc_df.loc[indicesToKeep, 'PC2'], c = color, s = 50)

for i, txt in enumerate(pc_df['id']):
    ax.annotate(txt, (pc_df['PC1'][i], pc_df['PC2'][i]))




