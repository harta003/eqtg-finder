# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 21:12:02 2022

@author: harta005
"""
import sys
import numpy as np
import scipy
import scipy.cluster.hierarchy as sch
import pandas as pd
import matplotlib.pylab as plt
from matplotlib import cm
import seaborn as sns
from matplotlib import cm
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import seaborn as sns; sns.set()
import os
os.chdir('C://Users/harta005/Projects/eQTG_Finder/plots')


# Main code
df = pd.read_csv('../data/eQTG_features_without_val_genes.csv')
cm = 1/2.54
fig, ax = plt.subplots(figsize=(18*cm,15*cm))
corr_matrix = df.corr().abs()
hm = sns.heatmap(corr_matrix, annot=False, vmin=0, vmax=1, linewidths=0, cmap="YlGnBu",
                 xticklabels=True, yticklabels=True)

# We change the fontsize of minor ticks label 
ax.tick_params(axis='both', which='major', labelsize=8)
ax.tick_params(axis='both', which='minor', labelsize=8)
cbar = ax.collections[0].colorbar
cbar.ax.tick_params(labelsize=8)
plt.xticks(fontsize=8)
plt.yticks(fontsize=8)

# BOld new features
for i in range(12):
    ax.get_xticklabels()[i].set_weight("bold")
    ax.get_yticklabels()[i].set_weight("bold")
    
for i in range(12, df.shape[1]-1):
    ax.get_xticklabels()[i].set_color('grey')
    ax.get_yticklabels()[i].set_color('grey')

fig.savefig('Supplementary_table1_correlation.pdf', format="pdf", dpi=350, bbox_inches='tight')
plt.show()