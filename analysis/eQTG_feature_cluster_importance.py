# -*- coding: utf-8 -*-
#!/usr/bin/python
"""
Created on Mon Jun 28 17:06:49 2021

The faster implementation of feature importance calculation by leave-one-out appraoch
Usage example: python eQTG_feature_importance.py -f eQTG_feature_without_test_genes.csv -n 40
"""

### Define comand-line arguments ###
import argparse
parser = argparse.ArgumentParser(description='The faster implementation of feature importance calculation by leave-one-out appraoch')
requiredArg = parser.add_argument_group('required arguments')
requiredArg.add_argument('-f', dest='filename', required=True, help='The path and name for the input feature files (csv)', type = str)
parser.add_argument('-n', dest= 'n_thread', help='The number of thread used to run the program', type=int)
parser.add_argument('-o', dest='output', help="The path and name for the output file", type=str)
args = parser.parse_args()

### Load libraries
import os
#os.chdir('C://Users/harta005/Projects/eQTG_Finder/analysis')
os.environ['OPENBLAS_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['OPENBLAS_MAIN_FREE'] = '1'
import numpy as np
import random
import pandas as pd
#from sklearnex import patch_sklearn # Speed up sklearn
#patch_sklearn()
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_curve, auc
from sklearn import ensemble
import time
from eQTG_tuning import load_feature_data, parallelize_function
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from scipy.spatial.distance import squareform

### Define functions ###
def clustering_feature(features, threshold=0.6):
    features = features.drop(['class'], axis=1)
    corr = features.corr().values
    corr = abs(corr)
    dissimilarity = 1 - corr
    Z = linkage(squareform(dissimilarity), method='complete')
    #dendrogram(Z, labels = features.columns, orientation='top', 
    #               color_threshold = 1 - threshold)
    labels = fcluster(Z, 1 - threshold, criterion='distance')
    cluster_df = pd.DataFrame({'feature' : features.columns, 'cluster' : labels})
    return cluster_df
    
def calculate_roc_loo_cluster(features, cluster, train_ratio = 50, test_ratio = 200, n_estimator = 300, min_samples_split = 2, max_features=4): # out should be a feature name
    print(f'now calculating the feature importance of {cluster}')
    random.seed(0)
    positive_set = features[features['class']==1]
    clf = ensemble.RandomForestClassifier(n_estimators=n_estimator,min_samples_split=min_samples_split, random_state=12, n_jobs=1)
    k_numb_iter=50 ### randomdize k-fold splitting
    neg_numb_iter=50 #randomdize the negatives selected
    aucs = []

    # Define the indices of negatives
    negative_indices = list(features[features['class']==0].index)

    if  k_numb_iter > 1 : # shuffle on or off based on k_iter
        shuffle_switch=True
    else: 
        shuffle_switch=False


    for n in range(0, k_numb_iter): # these interations are to randomdize k-fold splitting
        for i in range(0, neg_numb_iter):
            auc_mean=[]

            # Split negatives
            random.shuffle(negative_indices)
            train_len = 4/5 * positive_set.shape[0]
            test_len = 1/5 * positive_set.shape[0]
            no_train_negatives = int(train_len*train_ratio)
            no_test_negatives = int(test_len*test_ratio)
            training_neg_indices, test_neg_indices, _ = np.split(negative_indices, [no_train_negatives, no_train_negatives + no_test_negatives])
            training_negatives = features.iloc[training_neg_indices]
            test_negatives = features.iloc[test_neg_indices]
            skf= StratifiedKFold(n_splits=5,shuffle=shuffle_switch)

            for train_cv, test_cv in skf.split(positive_set.drop(['class'], axis=1,inplace=False), positive_set['class']): #emumerate is to adds a counter
                train_data = positive_set.iloc[train_cv] 
                test_data = positive_set.iloc[test_cv]

                train_data = pd.concat([train_data, training_negatives]) 
                test_data = pd.concat([test_data, test_negatives])
                
                train_feature=train_data.drop(['class'], axis=1)
                test_feature=test_data.drop(['class'], axis=1)
                train_feature_L=train_feature.drop(cluster, axis=1, inplace=False) # drop a feature
                test_feature_L=test_feature.drop(cluster, axis=1, inplace=False) # drop a feature
                
                clf = ensemble.RandomForestClassifier(n_estimators=n_estimator,min_samples_split=min_samples_split, max_features=max_features, random_state=0)
                clf.fit(train_feature, train_data['class'])
                probas_= clf.predict_proba(test_feature) # extract prob of a class (currently 50 test samples)
                
                clf = ensemble.RandomForestClassifier(n_estimators=n_estimator,min_samples_split=min_samples_split, max_features=max_features, random_state=0)
                clf.fit(train_feature_L, train_data['class'])
                probas_L= clf.predict_proba(test_feature_L)
                
                fpr, tpr1, _ = roc_curve(test_data['class'], probas_[:, 1]) # export precision, recall,  associated threshhold at each step
                fpr_L, tpr1_L, _ = roc_curve(test_data['class'], probas_L[:, 1]) # export precision, recall,  associated threshhold at each step
                auc_mean.append(auc(fpr_L, tpr1_L)-auc(fpr,tpr1)) #calculate AUC reduction
            aucs.append(np.mean(auc_mean))
    print([cluster, np.mean(aucs), np.std(aucs)])
    return ([cluster, np.mean(aucs), np.std(aucs)])

### Main code ###
if __name__ == "__main__":
    start_time = time.time()

    # Confirming arguments
    if args.n_thread is not None:
        n_thread = args.n_thread
    else:
        n_thread = 1
    if args.output is not None:
        out = args.output
    else:
        out = 'eQTG_feature_importance.csv'
    print(f'Using {n_thread} thread(s)')
    print(f'The result will be written as {out}')

    # Load the data
    features = load_feature_data(args.filename)
    
    # Define the cluster
    cluster_df  = clustering_feature(features)

    # Define the classifier parameters 
    train_ratio = 50
    test_ratio = 200
    n_estimator = 300
    min_samples_split = 2
    max_features = 6
    #df['network_weight'] = preprocessing.scale(df['network_weight'])

    # Create a combination of the parameters
    params = []
    feature_name = features.columns[:-1]
    for cluster_id in range(max(cluster_df['cluster'])):
        cluster = cluster_df[cluster_df['cluster'] == cluster_id + 1]['feature'].values.tolist()
        params.append([features, cluster])

    # Run the function in parallel
    print('Calculation in progress...')
    results = parallelize_function(calculate_roc_loo_cluster, params, n_thread)
    print('Finish!')
    print("--- %s seconds ---" % round((time.time() - start_time), 2) )

    # Arrange the data in a dataframe
    df = pd.DataFrame.from_records(results)
    df.columns = ['feature', 'mean AUC drop', 'std']

    # Write the dataframe
    df = df.sort_values('mean AUC drop').reset_index()
    df.to_csv(out, index=False)

    # # Figure Size
    # fig, ax = plt.subplots(figsize=(16, 9))
    
    # # Horizontal Bar Plot
    # ax.bar(df.feature, df['mean AUC drop'], yerr = df['std'])

    # # Indicate axis labels
    # plt.ylabel('AUC drop')
    # plt.xlabel('feature')

    # # Change the x tick axis
    # plt.xticks(rotation=90)
    # ax.set_aspect('1000')

    # # save Plot
    # plt.savefig('eQTG_feature_importance.png')