# -*- coding: utf-8 -*-
#!/usr/bin/python
"""
Created on Mon Jun 28 17:06:49 2021

The implementation of custom grid search cross-validation to determine the best parameter combination for model training
Usage example: python eQTG_plot_cv.py
"""

### Define comand-line arguments ###
import argparse
parser = argparse.ArgumentParser(description='The implementation of custom grid search cross-validation to determine the best parameter combination for model training')
parser.add_argument('-n', dest= 'n_thread', help='The number of thread used to run the program', type=int)
parser.add_argument('-o', dest='output', help="The path and name for the output file", type=str)
args = parser.parse_args()

### Load libraries ###
import os
# Following lines prevent numpy to use exxesive number of threads
os.environ['OPENBLAS_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['OPENBLAS_MAIN_FREE'] = '1'
import numpy as np
import random
import pandas as pd
import time
from itertools import product
import multiprocessing as mp
#from sklearnex import patch_sklearn # Speed up sklearn
#patch_sklearn()
from sklearn.model_selection import StratifiedKFold
from sklearn import ensemble
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import pickle

### Define functions ###
def calculate_tpr_auc(features, train_ratio, test_ratio, n_estimator, min_samples_split, max_features):
    clf = ensemble.RandomForestClassifier(n_estimators=n_estimator,min_samples_split=min_samples_split,max_features=max_features, random_state=0, n_jobs=40) # random forest parameters
    positive_set = features[features['class']==1]
    k_numb_iter=50 #  interations for randomly re-spliting causal gene list, 50
    neg_numb_iter=50 # iteration for randomly select negatives and re-train model, 50
    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)

    # Define the indices of negatives
    negative_indices = list(features[features['class']==0].index)

    if  k_numb_iter > 1 : # re-shuffle on or off based on k_iter
        shuffle_switch=True
    else: 
        shuffle_switch=False
    for n in range(0, k_numb_iter): #  re-spliting causal gene list for cross-validation
        print(f'iteration: {n+1}/{k_numb_iter}')
        
        for i in range(0, neg_numb_iter):
            auc_mean=[]

            # Split negatives
            random.shuffle(negative_indices)
            train_len = 4/5 * positive_set.shape[0]
            test_len = 1/5 * positive_set.shape[0]
            no_train_negatives = int(train_len*train_ratio)
            no_test_negatives = int(test_len*test_ratio)
            training_neg_indices, test_neg_indices, _ = np.split(negative_indices, [no_train_negatives, no_train_negatives + no_test_negatives])
            training_negatives = features.iloc[training_neg_indices]
            test_negatives = features.iloc[test_neg_indices]
            skf= StratifiedKFold(n_splits=5,shuffle=shuffle_switch)
            
            for train_cv, test_cv in skf.split(positive_set.drop(['class'], axis=1,inplace=False) ,positive_set['class']):
                train_data = positive_set.iloc[train_cv] 
                test_data = positive_set.iloc[test_cv] 

                train_data=train_data.append(training_negatives) 
                test_data=test_data.append(test_negatives)

                train_feature=train_data.drop(['class'], axis=1)
                test_feature=test_data.drop(['class'], axis=1)
                probas_=clf.fit(train_feature, train_data['class']).predict_proba(test_feature) #  Probability of falling into a class 
                #importance.append(clf.feature_importances_) # feature importance (Gini importance), not used 
                fpr, tpr1, thresholds1 = roc_curve(test_data['class'], probas_[:, 1]) # false positve rate, true positiverate, and treshhold
                interp_tpr = np.interp(mean_fpr, fpr, tpr1)
                interp_tpr[0] = 0.0
                tprs.append(interp_tpr)
                #aucs.append(auc(fpr, tpr1))
                auc_mean.append(auc(fpr, tpr1)) # AUC-ROC 
            aucs.append(np.mean(auc_mean))
    return (tprs, aucs)


def load_feature_data(filename):
    """Return the df of feature data and positive set from a given filename
    Args:
        filename (str): path to the csv containg feature data
    Returns:
        df (pandas df): gene features
        positive_set (pandas df): same as df but only contain positive genes
    """
    df = pd.read_csv(filename)
    df = df.drop(['ID'], axis=1)
    df = df.dropna(axis=1,how='all')
    return df

def plot_roc_auc(tprs1, aucs1, tprs2, aucs2, tprs3, aucs3, out):
    # Create the plot 
    cm = 1/2.54
    fig, ax = plt.subplots(figsize=(8*cm,6*cm))
    ax.plot([0, 1], [0, 1], linestyle='--', lw=.5, color='black',
            label='Chance', alpha=1)
    # Calculate mean TPS
    mean_fpr = np.linspace(0, 1, 100)
    mean_tpr = np.mean(tprs1, axis=0)
    mean_tpr2 = np.mean(tprs2, axis=0)
    mean_tpr3= np.mean(tprs3, axis=0)
    mean_tpr[-1] = 1.0
    mean_tpr2[-1] = 1.0
    mean_tpr3[-1] = 1.0
    # Calculate mean AUC
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs1)
    mean_auc2 = auc(mean_fpr, mean_tpr2)
    std_auc2 = np.std(aucs2)
    mean_auc3 = auc(mean_fpr, mean_tpr3)
    std_auc3 = np.std(aucs3)
    # Plot the mean TPR against FPR
    ax.plot(mean_fpr, mean_tpr, color='blue',
            label=r'QTG-Finder2 + new features (AUC = %0.2f $\pm$ %0.2f; n = 12500)' % (mean_auc, std_auc),
            lw=0.5, alpha=.8)
    ax.plot(mean_fpr, mean_tpr2, color='green',
            label=r'QTG-Finder2 (AUC = %0.2f $\pm$ %0.2f; n = 12500)' % (mean_auc2, std_auc2),
            lw=0.5, alpha=.8)
    ax.plot(mean_fpr, mean_tpr3, color='red',
            label=r'randomized class (AUC = %0.2f $\pm$ %0.2f; n = 12500)' % (mean_auc3, std_auc3),
            lw=0.5, alpha=.8)
    # Plot the SD
    std_tpr = np.std(tprs1, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    ax.fill_between(mean_fpr, tprs_lower, tprs_upper, color='blue', alpha=.05)
    std_tpr2 = np.std(tprs2, axis=0)
    tprs_upper2 = np.minimum(mean_tpr2 + std_tpr2, 1)
    tprs_lower2 = np.maximum(mean_tpr2 - std_tpr2, 0)
    ax.fill_between(mean_fpr, tprs_lower2, tprs_upper2, color='green', alpha=.05)
    std_tpr3 = np.std(tprs3, axis=0)
    tprs_upper3 = np.minimum(mean_tpr3 + std_tpr3, 1)
    tprs_lower3 = np.maximum(mean_tpr3 - std_tpr3, 0)
    ax.fill_between(mean_fpr, tprs_lower3, tprs_upper3, color='red', alpha=.05)
    

    # Set the lim, legend, and plot the figure
    # Set tick font size
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontsize(4)
    ax.set(xlim=[-0.05, 1.05], ylim=[-0.05, 1.05])
    ax.legend(loc="lower right", prop={"size":4})
    #plt.legend(frameon=False)
    plt.xlabel('False positive rate', fontsize=6)
    plt.ylabel('True positive rate', fontsize=6)
    fig.savefig(out, format="pdf", dpi=350, bbox_inches='tight')
    plt.show()



### Main code ###
if __name__ == "__main__":
    random.seed(0) # For reproducibility
    start_time = time.time()

    # Confirming arguments
    if args.n_thread is not None:
        n_thread = args.n_thread
    else:
        n_thread = 1
    if args.output is not None:
        out = args.output
    else:
        out = 'eQTG_cv.png'
    print(f'Using {n_thread} thread(s)')
    print(f'The result will be written as {out}')

    # Load the data
    #dt = 'data_features/Arabidopsis_features_eqtl_project_without_test_genes.csv' # input feature list
    #features1 = load_feature_data('eQTG_features.csv')
    features1 = load_feature_data('eQTG_features_without_val_genes.csv')
    #features2 = load_feature_data('QTG2_feature_without_test_genes.csv')
    features2 = load_feature_data('QTG2_features_without_val_genes.csv')
    #features3 = features2.drop(['normalized_nonsyn_SNP', 'std_exp_tiss', 'avg_exp_root_meristem', 'avg_exp_stem', 'TFBS_indel', 'TFBS_snp', 'is_kinase', 'avg_exp_male', 'avg_exp_apical_meristem', 'percent_absence', 'is_start_lost', 'avg_exp_roots', 'is_nucleotides_metabolism', 'avg_exp_seeds', 'is_amino_acids_metabolism'], axis=1)
    features3 = features1.copy()
    features3['class'] = np.random.permutation(features1['class'].values)


    # Define the classifier parameters for eQTG- and QTG-Finder
    train_ratio = 50
    test_ratio = 200
    n_estimator = 300
    min_samples_split = 2
    max_features = 6

    train_ratio2 = 20
    test_ratio2 = 200
    n_estimator2 = 200
    min_samples_split2 = 2
    max_features2 = 'auto'

    # Run the function
    print('Calculation in progress...')
    tprs1, aucs1 = calculate_tpr_auc(features1, train_ratio, test_ratio, n_estimator, min_samples_split, max_features)
    tprs2, aucs2 = calculate_tpr_auc(features2, train_ratio2, test_ratio2, n_estimator2, min_samples_split2, max_features2)
    tprs3, aucs3 = calculate_tpr_auc(features3, train_ratio, test_ratio, n_estimator, min_samples_split, max_features)
    file = open('cv_pickle.dat', 'wb')
    pickle.dump([tprs1, aucs1, tprs2, aucs2, tprs3, aucs3], file)
    file.close()
    print('Finish!')
    print("--- %s seconds ---" % round((time.time() - start_time),2) )

    # Create the plot
    plot_roc_auc(tprs1, aucs1, tprs2, aucs2, tprs3, aucs3, "eQTG_cv.pdf")#args.output)
