# -*- coding: utf-8 -*-
#!/usr/bin/python
"""
Created on Mon Jun 28 17:06:49 2021

The implementation of a custom grid search cross-validation to determine the best parameter combination for model training
Usage example: python3 eQTG_tuning.py -f ../data/eQTG_feature_without_test_genes.csv -n 40 -o eQTG_tuning_final.csv
"""

### Define comand-line arguments ###
import argparse
parser = argparse.ArgumentParser(description='The implementation of custom grid search cross-validation to determine the best parameter combination for model training')
requiredArg = parser.add_argument_group('required arguments')
requiredArg.add_argument('-f', dest='filename', required=True, help='The path and name for the input feature files (csv)', type = str)
parser.add_argument('-n', dest= 'n_thread', help='The number of thread used to run the program', type=int)
parser.add_argument('-o', dest='output', help="The path and name for the output file", type=str)
args = parser.parse_args()

### Load libraries ###
import os
# Following lines prevent numpy to use exxesive number of threads
os.environ['OPENBLAS_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['OPENBLAS_MAIN_FREE'] = '1'
import numpy as np
import random
import pandas as pd
import time
from itertools import product
import multiprocessing as mp
#from sklearnex import patch_sklearn # Speed up sklearn
#patch_sklearn()
from sklearn.model_selection import StratifiedKFold
from sklearn import ensemble
from sklearn.metrics import roc_curve, auc
from tqdm import tqdm

### Define functions ###
def calculate_roc(features, train_ratio, test_ratio, n_estimator, min_samples_split, max_features):
    positive_set = features[features['class']==1]
    clf = ensemble.RandomForestClassifier(n_estimators=n_estimator,min_samples_split=min_samples_split,max_features=max_features, random_state=0, n_jobs=1) # random forest parameters
    k_numb_iter=10 #  interations for randomly re-spliting causal gene list, 50
    neg_numb_iter=10 # iteration for randomly select negatives and re-train model, 50
    aucs = []
    tprs = []
    mean_fpr = np.linspace(0, 1, 100)
    if  k_numb_iter > 1 : # re-shuffle on or off based on k_iter
        shuffle_switch=True
    else: 
        shuffle_switch=False

    #Define the indices of negatives
    negative_indices = list(features[features['class']==0].index)

    for n in range(0, k_numb_iter): #  re-spliting causal gene list for cross-validation
        for i in range(0, neg_numb_iter):
            auc_mean=[]

            # Split negatives
            random.shuffle(negative_indices)
            train_len = 4/5 * positive_set.shape[0]
            test_len = 1/5 * positive_set.shape[0]
            no_train_negatives = int(train_len*train_ratio)
            no_test_negatives = int(test_len*test_ratio)
            training_neg_indices, test_neg_indices, _ = np.split(negative_indices, [no_train_negatives, no_train_negatives + no_test_negatives])
            training_negatives = features.iloc[training_neg_indices]
            test_negatives = features.iloc[test_neg_indices]
            skf= StratifiedKFold(n_splits=5,shuffle=shuffle_switch)


        skf= StratifiedKFold(n_splits=5,shuffle=shuffle_switch)
        for train_cv, test_cv in skf.split(positive_set.drop(['class'], axis=1,inplace=False) ,positive_set['class']): 
            for i in range(0, neg_numb_iter): # iterate for randomly selecting negatives. 
                train_data = positive_set.iloc[train_cv] 
                test_data = positive_set.iloc[test_cv] 

                # Split negatives
                random.shuffle(negative_indices)
                no_train_negatives = int(len(train_data)*train_ratio)
                no_test_negatives = len(test_cv)*test_ratio
                training_neg_indices, test_neg_indices, _ = np.split(negative_indices, [no_train_negatives, no_train_negatives + no_test_negatives])
                training_negatives = features.iloc[training_neg_indices]
                test_negatives = features.iloc[test_neg_indices]
                #training_negative = random.sample(list(features[features['class']==0].index), int(len(train_data)*train_ratio)) # randomly select training negatives from genome genes
                #testing_negatives = random.sample(list(features[features['class']==0].index), len(test_cv)*test_ratio) # randomly select testing negatives from genome genes, negative:positive=200:1
                train_data=train_data.append(training_negatives) 
                test_data=test_data.append(test_negatives)

                for train_cv, test_cv in skf.split(positive_set.drop(['class'], axis=1,inplace=False) ,positive_set['class']):
                    train_data = positive_set.iloc[train_cv] 
                    test_data = positive_set.iloc[test_cv] 

                    train_data=train_data.append(training_negatives) 
                    test_data=test_data.append(test_negatives)

                    train_feature=train_data.drop(['class'], axis=1)
                    test_feature=test_data.drop(['class'], axis=1)
                    probas_=clf.fit(train_feature, train_data['class']).predict_proba(test_feature) #  Probability of falling into a class 
                    #importance.append(clf.feature_importances_) # feature importance (Gini importance), not used 
                    fpr, tpr1, thresholds1 = roc_curve(test_data['class'], probas_[:, 1]) # false positve rate, true positiverate, and treshhold
                    interp_tpr = np.interp(mean_fpr, fpr, tpr1)
                    interp_tpr[0] = 0.0
                    tprs.append(interp_tpr)
                    #aucs.append(auc(fpr, tpr1))
                    auc_mean.append(auc(fpr, tpr1)) # AUC-ROC 
                aucs.append(np.mean(auc_mean)) 
    print(f'train_ratio = {train_ratio}, n_est = {n_estimator}, min_sample_split = {min_samples_split}, max_feat = {max_features}, AUC = {np.mean(aucs)}, SD = {np.std(aucs)}, n = {len(aucs)}')  
    return aucs.mean(), aucs.std()

def load_feature_data(filename):
    """Return the df of feature data and positive set from a given filename

    Args:
        filename (str): path to the csv containg feature data

    Returns:
        df (pandas df): gene features
        positive_set (pandas df): same as df but only contain positive genes
    """
    path = args.filename
    df = pd.read_csv(path)
    df = df.drop(['ID'], axis=1)
    df = df.dropna(axis=1,how='all')
    return df

def generate_parameter_combination(params):
    prod = list(product(*params))
    prod = [list(ele) for ele in prod]
    return prod

def parallelize_function(func, params_comb, n_thread):
    """[summary]

    Args:
        function ([type]): [description]
        params_comb ([type]): [description]

    Returns:
        [type]: [description]
    """
    with mp.Pool(n_thread) as pool:
        results = pool.starmap(func, params_comb)
    results = [list(ele) for ele in results]
    return results

### Main code ###
if __name__ == "__main__":
    random.seed(0) # For reproducibility
    start_time = time.time()

    # Confirming arguments
    if args.n_thread is not None:
        n_thread = args.n_thread
    else:
        n_thread = 1
    if args.output is not None:
        out = args.output
    else:
        out = 'eQTG_tuning_final.csv'
    print(f'Using {n_thread} thread(s)')
    print(f'The result will be written as {out}')

    # Load the data
    features = load_feature_data(args.filename)

    # Define the classifier parameters that will be evaluated
    train_ratio = [5, 10, 20, 50, 70, 100]
    test_ratio = [200]
    n_estimator = [100, 200, 300]
    min_samples_split = [2, 4, 6, 8]
    max_features = [2, 4, 6, 8]

    # Create a combination of the parameters
    params_comb = generate_parameter_combination(params)
    temp = [features]
    for i in range(len(params_comb)):
        params_comb[i] = temp + params_comb[i]

    # Run the function in parallel
    print('Tuning in progress...')
    results = parallelize_function(calculate_roc, params_comb, n_thread)
    print('Finish!')
    print("--- %s seconds ---" % round((time.time() - start_time),2) )

    # Arange the data in a dataframe
    df = pd.DataFrame.from_records(results)
    params_df = pd.DataFrame.from_records(np.array(params_comb)[:,1:])
    final_df = pd.concat([params_df, df], axis=1)
    print(final_df)
    final_df.columns = ["train_ratio", "test_ratio", "n_estimator", "min_samples_split", "max_features", "mean_auc", "sd_auc"]
        
    # Write the dataframe
    final_df.to_csv(out, index=False)
    
