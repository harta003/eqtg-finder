#!/usr/bin/python

"""
Created on Mon Jun 28 17:06:49 2021

The custom implementation of eQTL causal gene prediction
Usage example: python3 eQTG_Finder_predict.py -m eQTG_model_final.dat
"""

### Define comand-line arguments ###
import argparse
parser = argparse.ArgumentParser(description='The custom implementation of eQTL causal gene prediction')
requiredArg = parser.add_argument_group('required arguments')
requiredArg.add_argument('-m',dest='model',required=True,help='trained model to rank the genes')
parser.add_argument('-n', dest= 'n_thread', help='The number of thread used to run the program', type=int)
#parser.add_argument('-t', dest= 'threshold', help='The threshold used to classify the pos and neg class', type=float)
#parser.add_argument("--rmgene",dest='gene_removal',help="remove specific genes from training set,seperate genes by comma, only used for test")
args = parser.parse_args()

### Load libraries ###
import os
import time
os.environ['OPENBLAS_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['OPENBLAS_MAIN_FREE'] = '1'
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn' --> disable warnings
import numpy as np
import pickle
#from sklearnex import patch_sklearn # Speed up sklearn
#patch_sklearn()

### Define global variables ###
GENE_LOC = pd.read_csv('Arabidopsis_gene_location.csv')


### Define functions ###
def get_neighboring_genes(gene, interval):
    pos = GENE_LOC[GENE_LOC['id'] == gene]['loc'].item()
    start = pos - (interval/2)
    end = pos + (interval/2)
    chr = GENE_LOC[GENE_LOC['id'] == gene]['chr'].item()
    neighbors = GENE_LOC[(GENE_LOC['chr'] == chr) & (GENE_LOC['loc'] >= start) & (GENE_LOC['loc'] <= end)]['id'].tolist()
    return neighbors

def create_test_set(gene_list, feature_df):
    test_df = feature_df[feature_df['ID'].isin(gene_list)]
    causal_genes = []
    for index, row in test_df.iterrows():
        if row['class'] == 1:
            causal_genes.append(row["ID"])
            test_df.drop(index, inplace=True)
    unknown_genes = set(gene_list) - set(causal_genes) - set(test_df['ID'])
    #print(f'Known causal genes are excluded: {", ".join(causal_genes)}')
    #print(f'Unknown genes: {", ".join(unknown_genes)}')
    #print(f'Number of genes: {len(test_df)}')
    return test_df.drop(['class'], axis=1)
     

def calculate_eqtg_prob(test_df, model):
    #prob = model.predict(test_df.drop(['ID'], axis=1))#[:, 1]
    #prob = (model.predict_proba(test_df.drop(['ID'], axis=1))[:,1] >= threshold).astype(bool)
    prob = model.predict_proba(test_df.drop(['ID'], axis=1))[:,1]
    return prob

def get_gene_rank_freq(gene, model):
    df = pickle.load(PIK_F)
    gene_list = get_neighboring_genes(gene, 1000000)
    neg_iter = pickle.load(PIK_F)
    test_df = create_test_set(gene_list, df)
    prob_list = []
    for i in range(neg_iter):
        #print(i)
        prob = calculate_eqtg_prob(test_df, pickle.load(PIK_F))
        prob_list.append(prob)
    prob_df = pd.DataFrame(prob_list, columns=test_df['ID']).sum()
    prob_rank = prob_df.rank(ascending=False)
    prob_rank.to_csv('delete_this.csv')
    rank = float(prob_rank[prob_rank.index == gene].values)#/len(gene_list)
    freq = float(prob_df[prob_df.index == gene].values)/neg_iter
    total = len(gene_list)
    #max_rank = prob_rank.max()
    perc_rank = rank/total
    return rank, freq, total, perc_rank

### Main code ###
if __name__ == "__main__":
    start_time = time.time()

    # Confirming arguments
    # if args.threshold is not None:
    #     threshold = args.threshold
    # else:
    #     threshold = 0.5

    # Run the analysis
    #genes = ["AT3G19150", "AT1G22770", "AT2G28680", "AT5G61590", "AT3G61890", "AT3G13460", "AT2G25930", "AT1G63650", "AT4G00480", "AT3G26744", "AT2G24790", "AT5G41315", "AT4G00650", "AT1G16060", "AT1G66370", "AT5G63470", "AT2G26330", "AT5G02830", "AT2G47460", "AT1G22920", "AT3G57480" ,"AT5G23460", "AT1G50460"]
    #genes = ['AT1G22770']
    #genes = ["AT5G61590", "AT1G22770", "AT3G26744", "AT1G63700", "AT5G28640", "AT5G43250", "AT2G46680", "AT3G61890", "AT1G19220",
    #"AT2G28680", "AT5G02830", "AT3G25830", "AT2G39730", "AT3G13460", "AT5G28770", "AT3G20040", "AT2G47460", "AT1G11340", "AT3G47500", 
    #"AT1G16060", "AT5G54930", "AT1G20330", "AT5G10140", "AT1G04400", "AT2G26330", "AT2G25930", "AT5G43940", "AT1G12240"]

    genes =  ["AT5G61590", "AT1G22770", "AT3G26744", "AT5G63470", "AT3G61890", "AT2G28680", "AT5G02830", "AT3G13460", "AT1G50460", "AT1G63650", "AT5G41315",
      "AT4G00480", "AT2G47460", "AT3G57480", "AT1G16060", "AT1G22920", "AT1G66370", "AT3G19150", "AT2G24790", "AT5G23460", "AT2G25930", "AT2G26330", "AT4G00650", 
      "AT5G23010", "AT4G03050"] # eQTG
    genes = ["AT1G27320", "AT5G35750", "AT2G44990", "AT5G25980", "AT5G26000", "AT2G25450", "AT4G38970", "AT2G45650", "AT4G15920", 
    "AT3G19580", "AT1G69270"] # QTG
    for gene in genes:
        PIK_F = open(args.model, "rb")
        rank, freq, total, perc_rank = get_gene_rank_freq(gene, PIK_F)
        print(f'{gene}\t{round(rank, 3)}\t{total}\t{perc_rank}\t{freq}')
    print("--- %s seconds ---" % round((time.time() - start_time), 2) )


