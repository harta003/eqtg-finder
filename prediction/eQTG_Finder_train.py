#!/usr/bin/python

"""
Created on Mon Jun 28 17:06:49 2021

The custom implementation of grid search cross-validation to determine the best parameter combination for model training
Usage example: python3 eQTG_Finder_train.py -f eQTG_features.csv -n 20 -i 5000 -o eQTG_model_final.dat
"""

### Define comand-line arguments ###
import argparse
parser = argparse.ArgumentParser(description='QTL_Finder_v2.0')
requiredArg = parser.add_argument_group('required arguments')
requiredArg.add_argument('-f',dest='filename',required=True,help='The path and name for the input feature files (csv)')
parser.add_argument('-n', dest= 'n_thread', help='The number of thread used to run the program', type=int)
parser.add_argument("--rmgene",dest='gene_removal',help="remove specific genes from training set,seperate genes by comma, only used for test")
parser.add_argument("-o",dest='output',help="output file")
parser.add_argument("-i",dest='n_iter',help="the number of iteration")
args = parser.parse_args()

### Load libraries ###
import os
#from sklearnex import patch_sklearn # Speed up sklearn
#patch_sklearn()
# Following lines prevent numpy to use exxesive number of threads
os.environ['OPENBLAS_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['OPENBLAS_MAIN_FREE'] = '1'
import numpy as np
import random
import pandas as pd
from sklearn import ensemble
import time
import argparse
import pickle

### Define functions ###
def train_qtg(df, train_set, n_thread, neg_iter):
    print('start training')
    clf = ensemble.RandomForestClassifier(n_estimators=200, min_samples_split=2, max_features='auto', random_state=12, n_jobs=n_thread) # eQTG
    #clf = ensemble.RandomForestClassifier(n_estimators=300, min_samples_split=2, max_features=8, random_state=12, n_jobs=n_thread) # eQTG
    #clf = ensemble.RandomForestClassifier(n_estimators=200, min_samples_split=2, max_features='auto', n_jobs=20, random_state=12)
    #neg_iter=5000  # interrations for randomly selecting negatives from genome and re-training models 
    pickle.dump(neg_iter, pik_f)
    for i in range(0, neg_iter):
                print(i)
                train_data = train_set # positives used for training 
                training_negative = random.choices(list(df[df['class']==0].index), k=int(len(train_data)*20)) # randomly select negatives from genome genes 
                train_data=train_data.append(df.iloc[training_negative]) 
                train_feature=train_data.drop(['class'], axis=1) 
                clf.fit(train_feature, train_data['class'])  # model fitting
                pickle.dump(clf, pik_f)
    print('training complete')
    
#### check known QTL gene on QTLs, remove, reducant ID, run
if __name__ == '__main__':
    random.seed(12)
    if args.n_thread is not None:
        n_thread = args.n_thread
    else:
        n_thread=1
    if args.output is not None:
        output = args.output
    else:
        output="model.dat"
    if args.n_iter is not None:
        n_iter = int(args.n_iter)
    else:
        n_iter=5000
    with open(output, "wb") as pik_f:
        dt = args.filename # input feature list
        start_time = time.time()
        df = pd.read_csv(dt)
        #df = df.drop(['normalized_nonsyn_SNP', 'std_exp_tiss', 'avg_exp_root_meristem', 'avg_exp_stem', 'TFBS_indel', 'TFBS_snp', 'is_kinase', 'avg_exp_male', 'avg_exp_apical_meristem', 'percent_absence', 'is_start_lost', 'avg_exp_roots', 'is_nucleotides_metabolism', 'avg_exp_seeds', 'is_amino_acids_metabolism'], axis=1)
        if args.gene_removal!=None:
            rm_gene_entry=args.gene_removal
            rm_gene_list=rm_gene_entry.split(",")
            if len(rm_gene_list)>0: 
                for i in rm_gene_list: 
                    print(i)
                    index=df[df['ID']==i].index[0]
                    if df.iloc[index,-1]==1: 
                        df.iloc[index,-1]=0
        pickle.dump(df, pik_f)
        df=df.dropna(axis=1,how='all') 
        df = df.drop(['ID'], axis=1) 
        train_set = df[df['class']==1]
        train_qtg(df, train_set, n_thread, n_iter) # execute 

    print("--- %s seconds ---" % round((time.time() - start_time),2) ) 

