#!/usr/bin/python

"""
Created on Mon Jun 28 17:06:49 2021

The custom implementation of eQTL causal gene prediction
Usage example: python3 eQTG_finder_predict_all.py -m eQTG_model_final.dat
"""

### Define comand-line arguments ###
import argparse
parser = argparse.ArgumentParser(description='The custom implementation of eQTL causal gene prediction')
requiredArg = parser.add_argument_group('required arguments')
requiredArg.add_argument('-m',dest='model',required=True,help='trained model to rank the genes')
parser.add_argument('-n', dest= 'n_thread', help='The number of thread used to run the program', type=int)
#parser.add_argument('-t', dest= 'threshold', help='The threshold used to classify the pos and neg class', type=float)
#parser.add_argument("--rmgene",dest='gene_removal',help="remove specific genes from training set,seperate genes by comma, only used for test")
args = parser.parse_args()

### Load libraries ###
import os
import time
os.environ['OPENBLAS_NUM_THREADS'] = '1'
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['OPENBLAS_MAIN_FREE'] = '1'
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn' --> disable warnings
import numpy as np
import pickle
#from sklearnex import patch_sklearn # Speed up sklearn
#patch_sklearn()

### Define global variables ###
GENE_LOC = pd.read_csv('Arabidopsis_gene_location.csv')


### Define functions ###
def get_neighboring_genes(gene, interval):
    pos = GENE_LOC[GENE_LOC['id'] == gene]['loc'].item()
    start = pos - (interval/2)
    end = pos + (interval/2)
    chr = GENE_LOC[GENE_LOC['id'] == gene]['chr'].item()
    neighbors = GENE_LOC[(GENE_LOC['chr'] == chr) & (GENE_LOC['loc'] >= start) & (GENE_LOC['loc'] <= end)]['id'].tolist()
    return neighbors

def create_test_set(gene_list, feature_df):
    test_df = feature_df[feature_df['ID'].isin(gene_list)]
    causal_genes = []
    #for index, row in test_df.iterrows():
    #    if row['class'] == 1:
    #        causal_genes.append(row["ID"])
    #        test_df.drop(index, inplace=True)
    #unknown_genes = set(gene_list) - set(causal_genes) - set(test_df['ID'])
    #print(f'Known causal genes are excluded: {", ".join(causal_genes)}')
    #print(f'Unknown genes: {", ".join(unknown_genes)}')
    #print(f'Number of genes: {len(test_df)}')
    return test_df.drop(['class'], axis=1)
     

def calculate_eqtg_prob(test_df, model):
    prob = model.predict_proba(test_df.drop(['ID'], axis=1))[:,1]
    return prob

def get_gene_rank_freq(gene, model):
    df = pickle.load(PIK_F)
    gene_list = get_neighboring_genes(gene, 1000000)
    neg_iter = pickle.load(PIK_F)
    test_df = create_test_set(gene_list, df)
    prob_list = []
    for i in range(neg_iter):
        #print(i)
        prob = calculate_eqtg_prob(test_df, pickle.load(PIK_F))
        prob_list.append(prob)
    prob_df = pd.DataFrame(prob_list, columns=test_df['ID']).sum()
    prob_rank = prob_df.rank(ascending=False)
    rank = float(prob_rank[prob_rank.index == gene].values)/len(gene_list)
    freq = float(prob_df[prob_df.index == gene].values)/neg_iter
    return rank, freq

def get_gene_prob_all(gene_list, model):
    df = pickle.load(model)
    neg_iter = pickle.load(model)
    prob_list = []
    test_df = create_test_set(gene_list, df)
    for i in range(neg_iter):
        print(i)
        prob = calculate_eqtg_prob(test_df, pickle.load(model))
        prob_list.append(prob)
    prob_df = pd.DataFrame(prob_list, columns=test_df['ID']).mean()
    return prob_df

### Main code ###
if __name__ == "__main__":
    start_time = time.time()
    df = pd.read_csv("eQTG_features.csv")
    #df = df[df['class'] == 0]
    gene_list = list(df.ID)
    PIK_F = open(args.model, "rb")
    prob_df = get_gene_prob_all(gene_list, PIK_F)
    prob_df.to_csv('gene_prob.csv')
    prob_df['gene'] = gene_list
    print("--- %s seconds ---" % round((time.time() - start_time), 2) )


